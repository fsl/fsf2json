import enum

class analysisLevel(enum.Enum):
    Lower = 1
    Higher  = 2

class registrationSearchSpace(enum.Enum):
    NoSearch = 0
    Normal  = 90
    Full = 180

class affineType(enum.Enum):
    ThreeDOF = 3
    SixDOF = 6
    SevenDOF = 7
    NineDOF = 9
    TwelveDOF = 12

class affineTypeWithBBR(enum.Enum):
    ThreeDOF = 3
    SixDOF = 6
    SevenDOF = 7
    BBR = "BBR"
    TwelveDOF = 12

class sliceTimingMode(enum.Enum):
    RegularUp   = 1
    RegularDown = 2
    OrderFile   = 3
    TimingsFile = 4
    Interleaved = 5

class EVshape(enum.Enum):
    Square              = 0
    Sinusoid            = 1
    CustomOneEntry      = 2
    CustomThreeColumn   = 3
    Interaction         = 4
    Empty               = 10


class convolution(enum.Enum):
    Unconvolved             = 0
    Gaussian                = 1
    Gamma                   = 2
    DoubleGamma             = 3
    GammaBasis              = 4
    SineBasis               = 5
    FIRbasis                = 6
    DoubleGammaAlt          = 8
