#!/usr/bin/env fslpython
import json
import sys
import types
from collections import OrderedDict
from enum import Enum

import tclSchema
import featTypes as ft

class FSFfromJSON:

    def __init__(self):
        pass

    def typeToTcl(self, key, value, pType=str):
        if key is None:
            #Just write raw text
            self.outputFile.write(f'''{value}''')
            return
        try:
            if issubclass(pType,Enum):
                #If Enum keys are type.value
                #convertedType = pType(value).value
                #If Enum keys are type.name
                convertedType = pType[value].value
            elif pType is dict:
                convertedType = int(type(value) is pType)
            elif pType is list:
                convertedType = int(len(value))
            elif pType is bool:
                #Bools are a special case
                convertedType = int(pType(value))
            elif value is None and pType is str:
                convertedType = f'''""'''
            elif pType is str:
                convertedType = f'''"{pType(value)}"'''
            else:
                convertedType = pType(value)
            self.outputFile.write(f'''set {key:<25} {convertedType}\n''')
        except:
            raise Exception(f'''Key "{key}" cannot be set to "{value}"''')

    def expandDottedLookup(self, encodedKeys, settings):
        keys = encodedKeys.split('.',1)
        if len(keys) > 1:
            return self.expandDottedLookup(keys[1],settings[keys[0]])
        return settings[keys[0]]

    def tclFromSchema(self, schema, settings, labels = {}, offset=None):
        for key, values in schema.items():
            print(f'''Processing {key}''')
            if key not in settings:
                raise Exception(f'''Missing key: {key}''')
            #Overrides for "Special" keys
            if key is 'sliceTimingCorrection':
                if settings[key] is None:
                    self.typeToTcl("fmri(st)",0,int)
                else:
                    self.typeToTcl("fmri(st)",settings[key]['Mode'],ft.sliceTimingMode)
                    if settings[key]['Mode'] is "OrderFile" or settings[key]['Mode'] is "TimingsFile":
                        self.typeToTcl("fmri(st_file)",settings[key]['File'],str)
                continue
            if key is "temporalFiltering":
                if settings[key] is None:
                    self.typeToTcl("fmri(temphp_yn)",0,bool)
                    self.typeToTcl("fmri(templp_yn)",0,bool)
                else:
                    self.typeToTcl("fmri(temphp_yn)",settings[key]['applyHighPassFilter'],bool)
                    self.typeToTcl("fmri(templp_yn)",settings[key]['applyLowPassFilter'],bool)
                continue

            if values['tclKey'] is not None:
                self.typeToTcl(values['tclKey'].format(labels=labels),settings[key],values['type'])

            if isinstance(settings[key],dict) or isinstance(settings[key],list):
                #This is a container key so start new block in tcl
                self.typeToTcl(None,f'''#{key}\n''')
                #Now determine the subSchema type
                if isinstance(values['type'],types.FunctionType):
                    self.tclFromSchema(values['type'](settings),settings[key],labels)
                elif type(settings[key]) is list:
                    for nodeID in range(len(settings[key])):
                        self.tclFromSchema(values['typeSchema'],settings[key][nodeID],{'nodeID' : nodeID+1})
                elif type(settings[key]) is dict:
                    self.tclFromSchema(values['typeSchema'],settings[key])




    def structuredJSONtoFSF(self, settings=None, filename=None):
        with open(filename,'w') as self.outputFile:
            #print(self.expandDottedLookup("Common.TR",settings))
            self.typeToTcl(None,'''#Meta\n''')
            self.typeToTcl("fmri(version)",6.00,float)
            self.typeToTcl("fmri(analysis)",settings['Meta']['analysisLevel'],ft.analysisLevel)
            self.typeToTcl("fmri(level)",1,int)
            self.typeToTcl("fmri(stats_yn)",0,int)
            self.tclFromSchema(tclSchema.lower, settings)

class JSONfromFSF :
    #'Borrowed' from fsl.data.featanalysis
    def parseFSF(self, fileName=None):
        settings  = {}

        with open(fileName, 'rt') as f:

            for line in f.readlines():
                line = line.strip()

                if not line.startswith('set '):
                    continue

                tkns = line.split(None, 2)

                key = tkns[1].strip()
                val = tkns[2].strip(' \'"')

                settings[key] = val

        return settings

    def tclToType(self, tclField, tclJSON, pType=str):
        print("TCL")
        print(tclField)
        if tclField in tclJSON:
            try:
                rawValue = tclJSON[tclField]
                #If numeric convert, as pure number fields are always best treated that way
                if rawValue.isnumeric():
                    rawValue=float(rawValue)
                #Special overrides for tcl keys used to create dicts/lists
                if pType is dict:
                    pType = bool
                if pType is list:
                    pType = int
                convertedType = pType(rawValue)
                if isinstance(convertedType,Enum):
                    #TODO decide if return .value or .name - interacts with FSF writer above
                    convertedType = convertedType.name
                return convertedType
            except:
                raise Exception('tcl field "{}" value "{}" cannot be converted to {}'.format(tclField,tclJSON[tclField],pType))
        return None

    def subSchema(self,newSchema,settings):
        if type(newSchema) is types.FunctionType:
            return newSchema(settings)
        return newSchema

    def tclToSchema(self, schema, tclJSON, labels = {}):
        parsed = {}
        if schema is None:
            return None
        for key, values in schema.items():
            print(f'''Parsing {key}''')
            print(type(values['type']))
            parsedValue = None
            if values['tclKey'] is not None:
                parsedValue = self.tclToType(values['tclKey'].format(labels=labels),tclJSON,values['type'])
            if values['type'] is dict:
                if values['tclKey'] is None or parsedValue is True:
                    parsedValue = self.tclToSchema(values['typeSchema'],tclJSON,labels)
                else:
                    parsedValue = None
            elif values['type'] is list:
                parsedValue = [self.tclToSchema(values['typeSchema'],tclJSON,{'nodeID' : nodeID+1}) for nodeID in range(parsedValue)]
            elif isinstance(values['type'],types.FunctionType):
                print("HERE")
                parsedValue = self.tclToSchema(values['type'](parsed),tclJSON,labels)

            parsed[key] = parsedValue
        print(parsed)
        return parsed



    def schemaFromFSF(self, parsedFSF=None) :
        settings = { 'FEATsettings' : {} }
        subDictionary = settings['FEATsettings']

        #Create Meta subdictionary
        meta= {
            'SettingsVersion' : 0.1,
            'analysisLevel' : self.tclToType('fmri(level)',parsedFSF,ft.analysisLevel)
        }
        subDictionary['Meta'] = meta

        schema = None
        if meta['analysisLevel'] == "Lower":
            schema = tclSchema.lower

        subDictionary.update(self.tclToSchema(schema,parsedFSF))
        with open("design.json",'w') as outputFile:
            json.dump(settings,outputFile,indent=4,sort_keys=False)
        return settings


reader = JSONfromFSF()
parsedFSF = reader.parseFSF(sys.argv[1])
print(parsedFSF)
settings = reader.schemaFromFSF(parsedFSF)
writer = FSFfromJSON()
writer.structuredJSONtoFSF(settings['FEATsettings'],'test.fsf')
