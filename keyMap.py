import featTypes as ft

keyMap = [
    {
        "subDictionary" : "Common",
        "keys" : [
            {
                "tclKey" : "fmri(outputDir)",
                "pythonKey" : "outputDirectory",
                "type" : str,
                "required" : True
            },
            {
                "tclKey" : "fmri(tr)",
                "pythonKey" : "TR",
                "type" : float,
                "required" : True
            },
            {
                "tclKey" : "fmri(npts)",
                "pythonKey" : "totalVolumes",
                "type" : int,
                "required" : True
            },
            {
                "tclKey" : "fmri(paradigm_hp)",
                "pythonKey" : "temporalHighpassCutoff",
                "type" : float,
                "required" : True
            }
        ]
    },
    {
        "subDictionary" : "InputData",
        "keys" : [
            {
                "tclKey" : "fmri(multiple)",
                "pythonKey" : None,
                "type" : int,
                "required" : True,
                "function" : lambda settings : len(settings)
            },
            [
                {
                    "tclKey" : "feat_files({labels[nodeID]})",
                    "pythonKey" : "timeseriesImage",
                    "type" : str,
                    "required" : True
                },
                {
                    "tclKey" : "highres_files({labels[nodeID]})",
                    "pythonKey" : "structuralImage",
                    "type" : str,
                    "required" : False
                }
            ]
        ],
    },
    {
        "subDictionary" : "Preprocessing",
        "keys" : [
            {
                "subDictionary" : "Registration",
                 "keys" : [
                    {
                        "subDictionary" : "toStructural",
                        "keys" : [
                            {
                                "tclKey" : "fmri(reghighres_dof)",
                                "pythonKey" : "linearType",
                                "type" :ft.affineType,
                                "required" : True
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        "subDictionary" : "Statistics",
        "keys" : [
            {
                "tclKey" : "fmri(evs_orig)",
                "pythonKey" : None,
                "type" : int,
                "required" : True,
                "function" : lambda settings : len(settings)
            },
            [
                {
                    "tclKey" : "fmri(shape{labels[nodeID]})",
                    "pythonKey" : "shape",
                    "type" : ft.EVshape,
                    "required" : True
                },
                {
                    "tclKey" : "fmri(convolve{labels[nodeID]})",
                    "pythonKey" : "convolution",
                    "type" : ft.convolution,
                    "required" : True
                }
            ]
        ],


    }
]
