import featTypes as ft


commonSchema = {
    "outputDirectory" : {
        "tclKey" : "fmri(outputdir)",
        "type" : str,
    },
    "TR" : {
        "tclKey" : "fmri(tr)",
        "type" : float,
    },
    "totalVolumes" : {
        "tclKey" : "fmri(npts)",
        "type" : int,
    },
    "temporalHighpassCutoff" : {
        "tclKey" : "fmri(paradigm_hp)",
        "type" : float,
    }
}


subjectSchema = {
    "timeseriesImage" : {
        "tclKey" : "feat_files({labels[nodeID]})",
        "type" : str,
    },
    "structuralImage" : {
        "tclKey" : "highres_files({labels[nodeID]})",
        "type" : str,
    },
    "fieldmapImage" : {
        "tclKey" : "unwarp_files({labels[nodeID]})",
        "type" : str,
    },
    "magnitudeImage" : {
        "tclKey" : "unwarp_files_mag({labels[nodeID]})",
        "type" : str,
    },
    "expandedFunctionalImage" : {
        "tclKey" : "initial_highres_files({labels[nodeID]})",
        "type" : str,
    }
}


toExpandedFunctionalSchema = {
    "linearType" : {
        "tclKey" : "fmri(reginitial_highres_dof)",
        "type" : ft.affineType,
    },
    "searchSpace" : {
        "tclKey" : "fmri(reginitial_highres_search)",
        "type" : ft.registrationSearchSpace,
    }
}


toStructuralSchema = {
    "linearType" : {
        "tclKey" : "fmri(reghighres_dof)",
        "type" : ft.affineTypeWithBBR,
    },
    "searchSpace" : {
        "tclKey" : "fmri(reghighres_search)",
        "type" : ft.registrationSearchSpace,
    }
}


toStandardSchema = {
    "linearType" : {
        "tclKey" : "fmri(regstandard_dof)",
        "type" : ft.affineType,
    },
    "searchSpace" : {
        "tclKey" : "fmri(regstandard_search)",
        "type" : ft.registrationSearchSpace,
    },
    "standardImage" : {
        "tclKey" : "fmri(regstandard)",
        "type" : str,
    },
    "nonLinearRegistration" : {
        "tclKey": "fmri(regstandard_nonlinear_yn)",
        "type" : dict,
        "typeSchema": {
            "warpResolution" : {
                "tclKey": "fmri(regstandard_nonlinear_warpres)",
                "type" : float,
            }
        }
    }
}

registrationSchema = {
    "toExpandedFunctional" : {
        "tclKey": "fmri(reginitial_highres_yn)",
        "type" : dict,
        "typeSchema": toExpandedFunctionalSchema
    },
    "toStructural" : {
        "tclKey": "fmri(reghighres_yn)",
        "type" : dict,
        "typeSchema": toStructuralSchema
    },
    "toStandard" : {
        "tclKey": "fmri(reghighres_yn)",
        "type" : dict,
        "typeSchema": toStandardSchema
    }
}


fieldmapUnwarpingSchema = {
    "dwellTime" : {
        "tclKey" : "fmri(dwell)",
        "type" : float
    },
    "echoTime" : {
        "tclKey" : "fmri(te)",
        "type" : float
    },
    "signalLossThreshold" : {
        "tclKey" : "fmri(signallossthresh)",
        "type" : float
    },
    #TODO enum type
    "unwarpDirection" : {
        "tclKey" : "fmri(unwarp_dir)",
        "type" : str
    }
}


preprocessingSchema = {
    "volumesToDelete" : {
        "tclKey" : "fmri(ndelete)",
        "type" : int
    },
    "applyMotionCorrection" : {
        "tclKey" : "fmri(mc)",
        "type" : bool
    },
    "fieldMapUnwarping" : {
        "tclKey" : "fmri(regunwarp_yn)",
        "type" : dict,
        "typeSchema" : fieldmapUnwarpingSchema
    },
    "sliceTimingCorrection" :{
        "tclKey" : "fmri(st)",
        "type" : dict,
        "typeSchema" : {
            "Mode" : {
                "tclKey" : "fmri(st)",
                "type" : ft.sliceTimingMode
            },
            "File" : {
                "tclKey" : "fmri(st_file)",
                "type" : str,
                "requiredIf" : [
                    ("Mode","OrderFile"),
                    ("Mode","TimingsFile")
                ]
            }
        },
    },
    "applyBrainExtraction" : {
        "tclKey" : "fmri(bet_yn)",
        "type" : bool
    },
    "spatialSmoothingFWHM" : {
        "tclKey" : "fmri(smooth)",
        "type" : float
    },
    "applyIntensityNormalisation" : {
        "tclKey" : "fmri(norm_yn)",
        "type" : bool
    },
    "temporalFiltering" :{
        "tclKey" : None,
        "type" : dict,
        "typeSchema" : {
            "applyLowPassFilter" : {
                "tclKey" : "fmri(templp_yn)",
                "type" : bool
            },
            "applyHighPassFilter" : {
                "tclKey" : "fmri(temphp_yn)",
                "type" : bool
            }
        }
    },
    "applyPerfusionSubtraction" : {
        "tclKey" : "fmri(perfsub_yn)",
        "type" : bool
    },
    "runICAexploration" : {
        "tclKey" : "fmri(melodic_yn)",
        "type" : bool
    },
    "Registration" : {
        "tclKey" : None,
        "type" : dict,
        "typeSchema" : registrationSchema
    }
}


def shapeSchema(settings=None):
    match settings['shape']:
        case "Square":
            return  {
                "skip" : {
                    "tclKey" : "fmri(skip{labels[nodeID]})",
                    "type" : float
                }
            }
        case "Sinusoid":
            return  {
                "skip" : {
                    "tclKey" : "fmri(skip{labels[nodeID]})",
                    "type" : float
                }
            }
        case "CustomOneEntry" | "CustomThreeColumn":
            return {
                "file" : {
                    "tclKey" : "fmri(custom{labels[nodeID]})",
                    "type" : str
                }
            }
        case "Interaction":
            return None
        case _:
            return None


EVschema = {
    "shape" : {
        "tclKey" : "fmri(shape{labels[nodeID]})",
        "type" : ft.EVshape
    },
    "shapeSettings" : {
        "tclKey" : None,
        "type" : shapeSchema
    },
    "convolution" : {
        "tclKey" : "fmri(convolve{labels[nodeID]})",
        "type" : ft.convolution
    }
}


statisticsSchema = {
    "EVs" : {
        "tclKey" : "fmri(evs_orig)",
        "type" : list,
        "typeSchema" : EVschema
    }
}


#Full lower-level schema
lower = {
    "Common" : {
        "tclKey" : None,
        "type" : dict,
        "typeSchema" : commonSchema
    },
    "InputData" : {
        "tclKey" : "fmri(multiple)",
        "type" : list,
        "typeSchema" : subjectSchema
    },
    "Preprocessing" : {
        "tclKey" : "fmri(filtering_yn)",
        "type" : dict,
        "typeSchema" : preprocessingSchema
    },
    "Statistics" : {
        "tclKey" : "fmri(stats_yn)",
        "type" : dict,
        "typeSchema" : statisticsSchema
    }
}
